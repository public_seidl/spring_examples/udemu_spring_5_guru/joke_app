package sk.seidl.example.jokeapp.services;

/**
 * @author Matus Seidl (5+3)
 * 2017-11-30
 */
public interface JokeService {
    String getJoke();
}
